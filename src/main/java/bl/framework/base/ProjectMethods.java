package bl.framework.base;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import bl.framework.api.SeleniumBase;
import utils.ReadDataFromExcel;

public class ProjectMethods extends SeleniumBase {
	
	public String tcName, tcDesc, author, category, dataWbookName, dataSheetName;
	
	@BeforeSuite
	public void beforeSuite() {
		reportSetup();
	}
	
	@BeforeClass
	public void beforeClass() {
		initializeTest(tcName, tcDesc, author, category);		
	}
	
	@Parameters({"url","uname","pwd"})
	@BeforeMethod
	public void openBrowser(String url, String username, String password) {
		startApp("chrome", url);
		clearAndType(locateElement("id", "username"), username);
		clearAndType(locateElement("id", "password"), password);
		click(locateElement("class", "decorativeSubmit"));
		click(locateElement("linktext", "CRM/SFA"));
	}
	
	@AfterMethod
	public void closeBrowser() {
		close();
	}
	
	@AfterSuite
	public void fluchMemory() {
		saveReport();
	}
	
	@DataProvider(name="ActiveGp")
	public Object[][] getData() {
		return ReadDataFromExcel.readData(dataWbookName, dataSheetName);
	}
}
