package bl.framework.testcases;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;

public class FaceBookTestCase extends SeleniumBase{
	
	@BeforeSuite
	public void setupReportSpecs() {
		reportSetup();
	}
	
	@BeforeTest
	public void initTest() {
		initializeTest("TestCaseFB", "Automate FB", "Sathish", "Regression");		
	}
	
	@Test
	public void automateFB() {
		startApp("chrome", "https://www.facebook.com/");
		clearAndType(locateElement("xpath", "//input[@id='email']"), "mrskumar86");
		clearAndType(locateElement("xpath", "//input[@id='pass']"), "dhatcha2");
		click(locateElement("xpath", "//input[@data-testid='royal_login_button']"));
		clearAndType(locateElement("xpath", "//input[@data-testid='search_input']"), "TestLeaf");
		/*ChromeOptions op = new ChromeOptions();
		op.addArguments("--disable-notifications");
		ChromeDriver driver = new ChromeDriver(op);*/
		click(locateElement("xpath", "//button[@data-testid='facebar_search_button']"));
		click(locateElement("linktext", "Places"));
	}
	
	@AfterSuite
	public void flushMemory() {
		saveReport();
	}
}
