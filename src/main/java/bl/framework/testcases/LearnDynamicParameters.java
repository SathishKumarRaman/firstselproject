package bl.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bl.framework.base.ProjectMethods;

public class LearnDynamicParameters extends ProjectMethods{
	
	@BeforeTest
	public void beforetest() {
		tcName = "TestCase1";
		tcDesc = "First Test Case";
		author = "Sathish";
		category = "Integration";
		dataWbookName = "UseCases1";
		dataSheetName = "Active";
	}
	
	@Test(dataProvider = "ActiveGp")
	public void CreateLd(String cname, String fname, String lname)
	{
		click(locateElement("linktext", "Create Lead"));
		clearAndType(locateElement("id", "createLeadForm_companyName"), cname);
		clearAndType(locateElement("xpath", "//input[@id='createLeadForm_firstName']"), fname);
		clearAndType(locateElement("id", "createLeadForm_lastName"), lname);
		click(locateElement("name", "submitButton"));
	}

}
