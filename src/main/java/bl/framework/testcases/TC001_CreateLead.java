package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import bl.framework.base.ProjectMethods;

public class TC001_CreateLead extends ProjectMethods{
	
	@BeforeTest
	public void beforeTest() {
		tcName = "TestCase1";
		tcDesc = "First Test Case";
		author = "Sathish";
		category = "Integration";
	}

	@Test
	public void createLead() {

		WebElement eleLead = locateElement("linktext", "Create Lead");
		click(eleLead);
		WebElement eleCName = locateElement("id", "createLeadForm_companyName");
		clearAndType(eleCName, "Sastha Mosaics");
		/*WebElement eleParacc = locateElement("xpath", "//img[@alt='Lookup']");
		clickNoSnap(eleParacc);
		switchToWindow(1);
		WebElement eleFirstCell = locateElement("xpath", "//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']/div/a");
		clickNoSnap(eleFirstCell);
		switchToWindow(0);*/
		WebElement eleFName = locateElement("xpath", "//input[@id='createLeadForm_firstName']");
		clearAndType(eleFName, "Raman");
		WebElement eleLName = locateElement("id", "createLeadForm_lastName");
		clearAndType(eleLName, "Muniyappan");
		WebElement eleSlct = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(eleSlct, "Self Generated");
		WebElement eleCmp = locateElement("id", "createLeadForm_marketingCampaignId");
		selectDropDownUsingValue(eleCmp, "DEMO_MKTG_CAMP");
		clearAndType(locateElement("name", "firstNameLocal"), "Sastha");
		clearAndType(locateElement("name", "lastNameLocal"), "Raman");
		clearAndType(locateElement("name", "personalTitle"), "Managing Director");
		//clearAndType(locateElement("id", "createLeadForm_birthDate"), "14/04/86");
		clearAndType(locateElement("id", "createLeadForm_generalProfTitle"), "Mosaic Company");
		clearAndType(locateElement("id", "createLeadForm_departmentName"), "Flooring");
		clearAndType(locateElement("id", "createLeadForm_annualRevenue"), "500000");
        WebElement elecurr = locateElement("id", "createLeadForm_currencyUomId");
        selectDropDownUsingText(elecurr, "INR");
        WebElement eleIndus = locateElement("id", "createLeadForm_industryEnumId");
        selectDropDownUsingText(eleIndus, "Real");
        clearAndType(locateElement("id", "createLeadForm_numberEmployees"), "50");
        selectDropDownUsingValue(locateElement("id", "createLeadForm_ownershipEnumId"), "OWN_PROPRIETOR");
        clearAndType(locateElement("id", "createLeadForm_sicCode"), "123456");
        clearAndType(locateElement("id", "createLeadForm_tickerSymbol"), "###");
        clearAndType(locateElement("id", "createLeadForm_description"), "Quality-Honesty-Punctuality");
        clearAndType(locateElement("id", "createLeadForm_importantNote"), "Work with Satisfaction");
        clearAndType(locateElement("id", "createLeadForm_primaryPhoneCountryCode"), "+91");
        clearAndType(locateElement("id", "createLeadForm_primaryPhoneAreaCode"), "044");
        clearAndType(locateElement("id", "createLeadForm_primaryPhoneNumber"), "9940686883");
        clearAndType(locateElement("id", "createLeadForm_primaryPhoneExtension"), "43905096");
        clearAndType(locateElement("id", "createLeadForm_primaryPhoneAskForName"), "Sathish Kumar R");
        clearAndType(locateElement("id", "createLeadForm_primaryEmail"), "mrskumar86@gmail.com");
        clearAndType(locateElement("id", "createLeadForm_primaryWebUrl"), "www.google.com");
        clearAndType(locateElement("id", "createLeadForm_generalToName"), "Raman M");
        clearAndType(locateElement("id", "createLeadForm_generalAttnName"), "Sastha");
        clearAndType(locateElement("id", "createLeadForm_generalAddress1"), "No 163, Ganesa Puram");
        clearAndType(locateElement("id", "createLeadForm_generalAddress2"), "Thirukovilur Road");
        clearAndType(locateElement("id", "createLeadForm_generalCity"), "Tiruvannamalai");
        selectDropDownUsingText(locateElement("id", "createLeadForm_generalCountryGeoId"), "India");
        selectDropDownUsingText(locateElement("id", "createLeadForm_generalStateProvinceGeoId"), "TAMILNADU");
        clearAndType(locateElement("id", "createLeadForm_generalPostalCode"), "606601");
        clearAndType(locateElement("id", "createLeadForm_generalPostalCodeExt"), "606601");
        click(locateElement("name", "submitButton"));
        WebElement eleVCName = locateElement("id", "viewLead_companyName_sp");
        boolean result = verifyPartialText(eleVCName, "Sastha");
        if(result) System.out.println("Lead created successfully");
        else System.out.println("Lead not created yet");
	}

}








