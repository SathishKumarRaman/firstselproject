package bl.framework.testcases;

import org.testng.annotations.Test;

import bl.framework.base.ProjectMethods;

public class TC004_MergeLead extends ProjectMethods{
	
	@Test
	public void mergeLead() throws InterruptedException {
	click(locateElement("linktext", "Leads"));
	click(locateElement("linktext", "Merge Leads"));
	click(locateElement("xpath", "//img[@alt='Lookup']"));
	switchToWindow(1);
	clearAndType(locateElement("xpath", "//label[text()='Lead ID:']//following::div/input"), "10768");
	click(locateElement("xpath", "//button[text()='Find Leads']"));
	Thread.sleep(1000);
	clickNoSnap(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a"));
	switchToWindow(0);
	click(locateElement("xpath", "(//img[@alt='Lookup'])[2]"));
	switchToWindow(1);
	clearAndType(locateElement("xpath", "//label[text()='Lead ID:']//following::div/input"), "10767");
	click(locateElement("xpath", "//button[text()='Find Leads']"));
	Thread.sleep(1000);
	clickNoSnap(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a"));
	switchToWindow(0);
	clickNoSnap(locateElement("xpath", "//a[@class='buttonDangerous']"));
	driver.switchTo().alert().accept();
	//acceptAlert();
	click(locateElement("xpath", "//a[text()='Find Leads']"));
	clearAndType(locateElement("xpath", "//div[@class='x-form-item x-tab-item']/div/input"), "10768");
	click(locateElement("xpath", "//button[text()='Find Leads']"));
	Thread.sleep(1000);
	verifyExactText(locateElement("xpath", "//div[@class='x-paging-info']"), "No records to display");
	close();
	
	}

}
