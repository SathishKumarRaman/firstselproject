package bl.framework.testcases;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import bl.framework.base.ProjectMethods;

public class TC002_DupLead extends ProjectMethods{

	@Test
	public void CreateDupLead() {
		click(locateElement("linktext", "Leads"));
		click(locateElement("linktext", "Find Leads"));
		click(locateElement("xpath", "//span[text()='Email']"));
		clearAndType(locateElement("name", "emailAddress"), "mrskumar86@gmail.com");
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		WebElement elemnt1 = locateElement("xpath", "//td[@class='x-grid3-col x-grid3-cell x-grid3-td-firstName ']/div/a");
		String text = getElementText(elemnt1);
		click(elemnt1);
		System.out.println("Page Title is: "+ getTitle());
		click(locateElement("xpath", "//a[@class='subMenuButton']"));
		System.out.println(verifyTitle("Duplicate Lead"));
		click(locateElement("name", "submitButton"));
		boolean value = verifyExactText(locateElement("id", "viewLead_firstName_sp"), text);
		System.out.println("Verified the text and it is Duplicate or not : "+value);
		close();		
	}

}
