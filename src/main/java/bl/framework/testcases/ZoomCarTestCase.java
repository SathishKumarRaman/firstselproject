package bl.framework.testcases;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;

public class ZoomCarTestCase extends SeleniumBase{

	@Test
	public void zoomCar() {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.zoomcar.com/chennai");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementByLinkText("Start your wonderful journey").click();
		driver.findElementByXPath("(//div[@class='component-popular-locations']/div)[2]").click();
		driver.findElementByXPath("//button[@class='proceed']").click();
		// Get the current date
		Date date = new Date();
		//Get only the date (and not month, year, time etc)
		DateFormat sdf = new SimpleDateFormat("dd");
		// Get today's date
		String today = sdf.format(date);
		// Convert to integer and add 1 to it
		int tomorrow = Integer.parseInt(today)+1;
		String tomo = tomorrow+"";
		driver.findElementByXPath("//div[@class='text']//parent::div[contains(text(), '"+tomorrow+"')]").click();
		driver.findElementByXPath("//button[@class='proceed']").click();
		String daypicked = driver.findElementByXPath("//div[starts-with(@class,'day picked')]").getText().replaceAll("\\D", "");
		System.out.println(daypicked);
		System.out.println(daypicked.contains((tomo))?"End date Verified":"End Date Mismatch");
		driver.findElementByXPath("//button[@class='proceed']").click();
		List<WebElement> priceElementList = driver.findElementsByXPath("//div[@class='price']");
		List<String> priceList = new ArrayList<>();
		for (WebElement eachelement : priceElementList) {
			priceList.add(eachelement.getText().replaceAll("\\D",""));
		}
		System.out.println(priceList);
		String maxamount = Collections.max(priceList);
		System.out.println(maxamount);
		String brandName = driver.findElementByXPath("//div[contains(text(), '"+maxamount+"')]//parent::div//parent::div//parent::div/div/h3").getText();
		System.out.println(brandName);
		WebElement buynow = driver.findElementByXPath("//h3[contains(text(), '"+brandName+"')]//following::button");
		buynow.click();
		driver.close();
		
	}
}
