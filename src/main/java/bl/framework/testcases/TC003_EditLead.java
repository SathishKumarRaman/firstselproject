package bl.framework.testcases;

import org.testng.annotations.Test;

import bl.framework.base.ProjectMethods;

public class TC003_EditLead extends ProjectMethods{
	
	@Test
	public void editLead() throws InterruptedException {
	click(locateElement("linktext", "Leads"));
	click(locateElement("linktext", "Find Leads"));
	clearAndType(locateElement("xpath", "(//input[@name='firstName'])[3]"), "Raman");
	click(locateElement("xpath", "//button[text()='Find Leads']"));
	Thread.sleep(1000);
	click(locateElement("xpath", "//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']/div/a"));
	verifyTitle("View Lead");
	click(locateElement("xpath", "//a[text()='Edit']"));
	clearAndType(locateElement("id", "updateLeadForm_companyName"), "Sastha Builder100");
	click(locateElement("name", "submitButton"));
	verifyPartialText(locateElement("id", "viewLead_companyName_sp"), "Sastha Builder100");
	close();
	}
}
