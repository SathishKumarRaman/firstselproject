package bl.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import bl.framework.base.ProjectMethods;

public class Sample extends ProjectMethods{
	
	@BeforeTest
	public void beforetest() {
		tcName = "TestCase1";
		tcDesc = "First Test Case";
		author = "Sathish";
		category = "Integration";
	}
	
	@Test(dataProvider = "ActiveGp")
	public void CreateLd(String cname, String fname, String lname)
	{
		click(locateElement("linktext", "Create Lead"));
		clearAndType(locateElement("id", "createLeadForm_companyName"), cname);
		clearAndType(locateElement("xpath", "//input[@id='createLeadForm_firstName']"), fname);
		clearAndType(locateElement("id", "createLeadForm_lastName"), lname);
		click(locateElement("name", "submitButton"));
	}

	@DataProvider(name = "ActiveGp")
	public String[][] fetchdata() {
		String[][] data = new String[2][3];
		data[0][0] = "Wipro";
		data[0][1] = "Sathish";
		data[0][2] = "Raman";
		
		data[1][0] = "Wipro";
		data[1][1] = "Boopalan";
		data[1][2] = "Raman";
		
		return data;
	}
	
	@DataProvider(name = "RetireGp")
	public String[][] fetchdata1() {
		String[][] data = new String[2][3];
		data[0][0] = "Wipro";
		data[0][1] = "Dhatcha";
		data[0][2] = "Sathish";
		
		data[1][0] = "Wipro";
		data[1][1] = "Sowmya";
		data[1][2] = "Boopalan";
		
		return data;
	}
}
