package bl.framework.api;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.InvalidSelectorException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.UnexpectedTagNameException;

import bl.framework.base.Browser;
import bl.framework.base.Element;
import utils.AdvancedReport;

public class SeleniumBase extends AdvancedReport implements Browser, Element{

	public static RemoteWebDriver driver;
	public int i =1;
	public String text = null;
	public String text1 = null;
	public String title1 = null;
	@Override
	public void startApp(String url) {
		// TODO Auto-generated method stub

	}

	@Override
	public void startApp(String browser, String url) {
		
			try {
				if(browser.equalsIgnoreCase("chrome")) {
					System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
					driver = new ChromeDriver();
				} else if(browser.equalsIgnoreCase("firefox")) {
					System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
					driver = new FirefoxDriver(); 
				}
				driver.manage().window().maximize();
				driver.get(url);
				
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				logOutPut("pass","The browser "+browser+" launched successfully");
			} catch (NullPointerException e) {
				System.err.println("Driver for the given browser not found");
			}catch (IllegalStateException e) {
				System.err.println("incorrect path to access the given driver");
			}catch (RuntimeException e) {
				System.err.println("Something went wrong while finding the driver for given browser");
			}
			finally {
			        takeSnap();
			}
	}

	@Override
	public WebElement locateElement(String locatorType, String value) {
		
		try {
			switch (locatorType) {
			case "id": return driver.findElementById(value);
			case "name": return driver.findElementByName(value);
			case "class": return driver.findElementByClassName(value);
			case "xpath": return driver.findElementByXPath(value);
			case "linktext": return driver.findElementByLinkText(value);
			case "pLinkText": return driver.findElementByPartialLinkText(value);
			case "tagName": return driver.findElementByTagName(value);
			case "CssS": return driver.findElementByCssSelector(value);
			default: return null;
			}
		}catch (InvalidSelectorException e) {
			System.err.println("Please verify the xpath or the element value has space");
	    }catch (NoSuchElementException e) {
			System.err.println("locator value is incorrect or not found");
		}catch (UnexpectedTagNameException e) {
				System.err.println("Please verify the tag name is acccurate");
		}catch (StaleElementReferenceException e) {
			System.err.println("Element may have deleted or not available at present");
	    }catch (NoSuchWindowException e) {
			System.err.println("Window not found or alreay closed");
		}catch (RuntimeException e) {
			System.err.println("Something went wrong while locating the element");
		}
		return null;
		
	}

	@Override
	public WebElement locateElement(String value) {
		try {
			return driver.findElementById(value);
		} catch (NoSuchElementException e) {
			System.err.println("locator value is incorrect or not found");
		}catch (RuntimeException e) {
			System.err.println("Something went wrong while locating the element");
		}
	return null;
	}

	@Override
	public List<WebElement> locateElements(String type, String value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void switchToAlert() {
		try {
			driver.switchTo().alert();
			logOutPut("pass", "Switched to Alert successfully");
		} catch (NoAlertPresentException e) {
			System.err.println("There is no alert to handle at this moment");
		} catch (RuntimeException e) {
			System.err.println("Something went wrong while locating the element");
		}
	}

	@Override
	public void acceptAlert() {
		try {
			driver.switchTo().alert().accept();
			logOutPut("pass", "Alert accepted successfully");
		} catch (NoAlertPresentException e) {
			System.err.println("There is no alert to handle at this moment");
		} catch (RuntimeException e) {
			System.err.println("Something went wrong while locating the element");
		}
	}

	@Override
	public void dismissAlert() {
		try {
			driver.switchTo().alert().dismiss();
			logOutPut("pass", "Alert declined successfully");
		} catch (NoAlertPresentException e) {
			System.err.println("There is no alert to handle at this moment");
		} catch (RuntimeException e) {
			System.err.println("Something went wrong while locating the element");
		}
	}

	@Override
	public String getAlertText() {
		try {
			return driver.switchTo().alert().getText();
		} catch (NoAlertPresentException e) {
			System.err.println("There is no alert to handle at this moment");
		} catch (RuntimeException e) {
			System.err.println("Something went wrong while locating the element");
		}
		return null;
	}

	@Override
	public void typeAlert(String data) {
		try {
			driver.switchTo().alert().sendKeys(data);
			logOutPut("pass", "Alert text entered successfully");
		} catch (NoAlertPresentException e) {
			System.err.println("There is no alert to handle at this moment");
		} catch (RuntimeException e) {
			System.err.println("Something went wrong while locating the element");
		}
	}

	@Override
	public void switchToWindow(int index) {
		try {
			Set<String> windowHandles = driver.getWindowHandles();
			List<String> lst = new ArrayList<>(windowHandles);
			driver.switchTo().window(lst.get(index));
			logOutPut("pass", "Switched to required window successfully");
		} catch (IndexOutOfBoundsException e) {
			System.err.println("index not found");
		}catch (NoSuchWindowException e) {
			System.err.println("no such window found or it might have already closed");
		}catch (RuntimeException e) {
			System.err.println("Something went wrong while locating the window");
		}
		finally {
			takeSnap();
		}
	}

	@Override
	public void switchToWindow(String title) {
		// TODO Auto-generated method stub

	}

	@Override
	public void switchToFrame(int index) {
		// TODO Auto-generated method stub

	}

	@Override
	public void switchToFrame(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void switchToFrame(String idOrName) {
		// TODO Auto-generated method stub

	}

	@Override
	public void defaultContent() {
		try {
			driver.switchTo().defaultContent();
			logOutPut("pass", "Switched to default window successfully");
		}catch (IndexOutOfBoundsException e) {
			System.err.println("index not found");
		}catch (NoSuchWindowException e) {
			System.err.println("no such window found or it might have already closed");
		}catch (RuntimeException e) {
			System.err.println("Something went wrong while locating the window");
		}
	}

	@Override
	public boolean verifyUrl(String url) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean verifyTitle(String title) {
		try {
			title1 = driver.getTitle();
			if(title1.contains(title)) 
			{
				System.out.println("Verified the title has: "+title); 
				logOutPut("pass", "Title verified successfully");
				return true;
			}
				
			else
			{
				System.out.println("Veified the title is not having : "+title); 
				logOutPut("fail", "Title verified successfully");
			    return false;
			}
		} catch (WebDriverException e) {
			System.err.println("Something went wrong while verifying the title");
		} catch (RuntimeException e) {
			System.err.println("Something went wrong while verifying the title");
		}
		 return false;
	}
	
	public String getTitle() {
		try {
			return driver.getTitle();
		} catch (WebDriverException e) {
			System.err.println("Something went wrong while verifying the title");
		} catch (RuntimeException e) {
			System.err.println("Something went wrong while verifying the title");
		}
		return null;
	}

	@Override
	public void takeSnap() {
		try {
			File src = driver.getScreenshotAs(OutputType.FILE);
			File des = new File("./snaps/img"+i+".png");
			try {
				FileUtils.copyFile(src, des);
				}catch (IOException e) {
				e.printStackTrace();
				}
			i++;
			}catch (NoSuchWindowException e) {
			System.err.println("window is not present or may already closed");
			} catch (WebDriverException e) {
			System.err.println("Something went wrong while taking snap of the window");
			}
	}

	@Override
	public void close() {
		try {
			driver.close();
			logOutPut("pass", "Window closed successfully");
		}catch (NoSuchWindowException e) {
		System.err.println("window is not present or may already closed");
		} catch (WebDriverException e) {
		System.err.println("Something went wrong on the window");
		}
	}

	@Override
	public void quit() {
		// TODO Auto-generated method stub

	}

	@Override
	public void click(WebElement ele) {
		try {
			ele.click();
			logOutPut("pass", "The element "+ele+" clicked successfully");
		} catch (NullPointerException e) {
			System.err.println("web element is Null");
		}catch (NoSuchElementException e) {
			System.err.println("web element is incorrect or not found");
			
		}catch (RuntimeException e) {
			System.err.println("Something went wrong with the element");
		}
		finally {
		takeSnap();
		}
	}
	
	public void clickNoSnap(WebElement ele) {
		try {
			ele.click();
			logOutPut("pass", "The element "+ele+" clicked successfully");
		} catch (NullPointerException e) {
			logOutPut("fail", "web element is Null");
		}catch (NoSuchElementException e) {
			System.err.println("web element is incorrect or not found");
		}catch (RuntimeException e) {
			System.err.println("Something went wrong with the element");
		}
		
	}

	@Override
	public void append(WebElement ele, String data) {
		try {
			ele.sendKeys(data);
			logOutPut("pass", "Text appended successfully");
		} catch (NullPointerException e) {
			System.err.println("web element is Null");
		}catch (NoSuchElementException e) {
			System.err.println("web element is incorrect or not found");
		}catch (RuntimeException e) {
			System.err.println("Something went wrong with the element");
		}
		finally {
			takeSnap();
		}
	}

	@Override
	public void clear(WebElement ele) {
		try {
			ele.clear();
			logOutPut("pass", "Text field cleared successfully");
		} catch (NullPointerException e) {
			System.err.println("web element is Null");
		}catch (NoSuchElementException e) {
			System.err.println("web element is incorrect or not found");
		}catch (RuntimeException e) {
			System.err.println("Something went wrong with the element");
		}
		finally {
			takeSnap();
		}		
	}

	@Override
	public void clearAndType(WebElement ele, String data) {
		try {
			ele.clear();
			ele.sendKeys(data, Keys.TAB); 
			logOutPut("pass", "The data "+data+" entered successfully");
			} catch (NullPointerException e) {
			System.err.println("web element is Null");
		}catch (NoSuchElementException e) {
			System.err.println("web element is incorrect or not found");
		}catch (RuntimeException e) {
			System.err.println("Something went wrong with the element");
		}
		finally {
			takeSnap();
		}
	}

	@Override
	public String getElementText(WebElement ele) {
		 try {
			text1 = ele.getText();
			System.out.println(text1);
			return text1;
		} catch (NullPointerException e) {
			System.err.println("web element is Null");
		}catch (NoSuchElementException e) {
			System.err.println("web element is incorrect or not found");
		}catch (RuntimeException e) {
			System.err.println("Something went wrong with the element");
		}
		 return null;
	}

	@Override
	public String getBackgroundColor(WebElement ele) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTypedText(WebElement ele) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
	    try {
			Select slct = new Select(ele);
			List<WebElement> options = slct.getOptions();
			for (WebElement webElement : options) {
				if(webElement.getText().startsWith(value)) {
					slct.selectByIndex(options.indexOf(webElement));
					break;
				}
			}
		logOutPut("pass", "DropDown Text Selected successfully");
		} catch (NullPointerException e) {
			System.err.println("Web element is Null");	
		}catch (NoSuchElementException e) {
			System.err.println("element is incorrect or not found");
		}catch (StaleElementReferenceException e) {
			System.err.println("The element you are looking for is not found in the dropldown list");
		}catch (RuntimeException e) {
			System.err.println("Something went wrong with the element");
		}
	    finally {
	    	takeSnap();
	    }
	    
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		try {
			Select slct = new Select(ele);
			slct.selectByIndex(index);
			logOutPut("pass", "DropDown Index Selected successfully");
		} catch (NullPointerException e) {
			System.err.println("Web element is Null");	
		}catch (NoSuchElementException e) {
			System.err.println("element is incorrect or not found");
		}catch (StaleElementReferenceException e) {
			System.err.println("The element you are looking for is not found in the dropldown list");
		}catch (RuntimeException e) {
			System.err.println("Something went wrong with the element");
		}
	    finally {
	    	takeSnap();
	    }
	}

	@Override
	public void selectDropDownUsingValue(WebElement ele, String value) {
		try {
			Select slct = new Select(ele);
			slct.selectByValue(value);
			logOutPut("pass", "DropDown Value Selected successfully");
		} catch (NullPointerException e) {
			logOutPut("fail", "Web element is Null");	
		}catch (NoSuchElementException e) {
			System.err.println("element is incorrect or not found");
		}catch (StaleElementReferenceException e) {
			System.err.println("The element you are looking for is not found in the dropldown list");
		}catch (RuntimeException e) {
			System.err.println("Something went wrong with the element");
		}
	    finally {
	    	takeSnap();
	    }	
	}

	@Override
	public boolean verifyExactText(WebElement ele, String expectedText) {
		try {
			text = ele.getText();
			if(text.equalsIgnoreCase(expectedText))
			{
				logOutPut("pass", "Yes:  "+text+" is same as expected : "+expectedText);
				return true;
			}
			else
			{
				logOutPut("fail", "No: "+text+" is not same as expected : "+expectedText);
				return false;
			}
		} catch (NullPointerException e) {
			System.err.println("Web element is Null");	
		}catch (NoSuchElementException e) {
			System.err.println("element is incorrect or not found");
		}catch (RuntimeException e) {
			System.err.println("Something went wrong with the element");
		}
	    finally {
	    	takeSnap();
	    }	
		return false;
	}

	@Override
	public boolean verifyPartialText(WebElement ele, String expectedText) {
		try {
			text = ele.getText();
			if(text.contains(expectedText))	{
				logOutPut("pass", "Expacted Text is: "+expectedText+" Matching with Actual Text: "+ text);
				return true;
			}
			else {
				logOutPut("fail", "Expacted Text is: "+expectedText+" Not Matching with Actual Text is: "+text);
				return false;
			}
		} catch (NullPointerException e) {
			System.err.println("Web element is Null");	
		}catch (NoSuchElementException e) {
			System.err.println("element is incorrect or not found");
		}catch (RuntimeException e) {
			System.err.println("Something went wrong with the element");
		}
	    finally {
	    	takeSnap();
	    }	
    	return false;
	}

	@Override
	public boolean verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifyDisappeared(WebElement ele) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifyEnabled(WebElement ele) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifySelected(WebElement ele) {
		// TODO Auto-generated method stub
		return false;
	}

}
