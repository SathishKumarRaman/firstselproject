package utils;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadDataFromExcel {

	static Object[][] data;
	public static Object[][] readData(String dataWbookName, String dataSheetName){
		try {
			XSSFWorkbook wbook = new XSSFWorkbook("./DataContainer/"+dataWbookName+".xlsx");
			XSSFSheet sheet = wbook.getSheet(dataSheetName);
			int lastRowNum = sheet.getLastRowNum();
			short lastCellNum = sheet.getRow(0).getLastCellNum();
			data = new Object[lastRowNum][lastCellNum];
			for (int j = 1; j <= lastRowNum; j++) {
				XSSFRow row = sheet.getRow(j);
			for (int i = 0; i < lastCellNum; i++) {
				data[j-1][i] = row.getCell(i).getStringCellValue();
			}
			}
			wbook.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return data;

	}

}
