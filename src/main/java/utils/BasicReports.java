package utils;

import java.io.IOException;

import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class BasicReports {
	static ExtentHtmlReporter html;
	static ExtentReports report;
	ExtentTest	test;
	
	
	public void reportSetup() {
		html = new ExtentHtmlReporter("./reports/extentReport.html");
		report = new ExtentReports();
		html.setAppendExisting(true);
		report.attachReporter(html);
	}
	

	public void initializeTest(String tcName, String tcDesc, String author, String category) {
		test = report.createTest(tcName, tcDesc);
		test.assignAuthor(author);
		test.assignCategory(category);
	}
	
	public void verifyTestResult() {
		
		
		
		//MediaEntity will pull the snap from given location and post it on the report, 
		// (./../snaps or snap is for going back to the previous level
		// .build() is to build the file and attach it with result string
		// it will throw io exception and asking to add try catch block to handle the file not found exception
		try {
			test.pass("First Test Case ran successfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		} catch (IOException e) {
			System.err.println("img file not found exception occured");
		}
		
	}
	
	public void saveReport() {
		report.flush();
	}
}
