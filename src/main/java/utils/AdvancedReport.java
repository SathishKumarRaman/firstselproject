package utils;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;


public class AdvancedReport {

	static ExtentHtmlReporter html;
	static ExtentReports report;
	ExtentTest	test;
	
	
	public void reportSetup() {
		html = new ExtentHtmlReporter("./reports/extentReport.html");
		report = new ExtentReports();
		html.setAppendExisting(true);
		report.attachReporter(html);
	}
	

	public void initializeTest(String tcName, String tcDesc, String author, String category) {
		test = report.createTest(tcName, tcDesc);
		test.assignAuthor(author);
		test.assignCategory(category);
	}
	
	public void logOutPut(String status, String resultText) {
		if(status.equalsIgnoreCase("pass")) {
			test.pass(resultText);
		}else if(status.equalsIgnoreCase("fail")) {
			test.fail(resultText);
		throw new RuntimeException();
		}else if(status.equalsIgnoreCase("warning")) {
			test.warning(resultText);
		}
		
	}
	
	public void saveReport() {
		report.flush();
	}
}
