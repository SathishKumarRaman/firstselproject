package directCdForTCs;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


public class DirectCodeforTC002 {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("//span[text()='Email']").click();
		driver.findElementByName("emailAddress").sendKeys("mrskumar86@gmail.com");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(2000);
		WebElement elemnt1 = driver.findElementByXPath("//td[@class='x-grid3-col x-grid3-cell x-grid3-td-firstName ']/div/a");
		String text = elemnt1.getText();
		System.out.println(text);
		elemnt1.click();
		System.out.println(driver.getTitle());
		driver.findElementByXPath("//a[@class='subMenuButton']").click();
		String title1 = driver.getTitle();
		if(title1.contains("Duplicate Lead"))
		System.out.println("Verified the title is: "+title1);
		else
			System.out.println("Veified the tile is not Duplicate Lead but it is:"+title1);
		driver.findElementByName("submitButton").click();
		String text2 = driver.findElementById("viewLead_firstName_sp").getText();
		if(text.equalsIgnoreCase(text2))
			System.out.println("Yes, Duplicate lead Name: "+text2+" is same as existing lead name: "+text);
		else
			System.out.println("No, Duplicate lead Name: "+text2+" is not same as existing lead name: "+text);
		//driver.findElementByXPath("//a[@class='subMenuButtonDangerous']").click();
		driver.close();
		
	}

}
