package directCdForTCs;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class DirectCodeForTC001 {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
	    driver.findElementById("createLeadForm_companyName").sendKeys("Sastha Mosaics");
	    
	    driver.findElementByXPath("//img[@alt='Lookup']").click();
	    
	    Set<String> windowHandles = driver.getWindowHandles();
	    List<String> lst = new ArrayList<>(windowHandles);
	    driver.switchTo().window(lst.get(1));
	    driver.findElementByXPath("//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']/div/a").click();
	    
	    driver.switchTo().window(lst.get(0));
	    driver.findElementById("createLeadForm_firstName").sendKeys("Sastha");
	    driver.findElementById("createLeadForm_lastName").sendKeys("Raman");
	     
	    WebElement source = driver.findElementById("createLeadForm_dataSourceId");
	    Select slct1 = new Select(source);
	    slct1.selectByVisibleText("Self Generated");
	     
	    WebElement marketing = driver.findElementById("createLeadForm_marketingCampaignId");
	    Select slct2 = new Select(marketing);
	    slct2.selectByValue("DEMO_MKTG_CAMP");
	    
	    driver.findElementByName("firstNameLocal").sendKeys("Sastha");
	    driver.findElementByName("lastNameLocal").sendKeys("Raman");
	    driver.findElementByName("personalTitle").sendKeys("Managing Director");
	    driver.findElementById("createLeadForm_birthDate").sendKeys("14/04/86");
	    driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Mosaic Company");
	    driver.findElementById("createLeadForm_departmentName").sendKeys("Flooring");
	    driver.findElementById("createLeadForm_annualRevenue").sendKeys("500000");
	    
	     
	    WebElement currency = driver.findElementById("createLeadForm_currencyUomId");
	    Select slct4 = new Select(currency);
	    List<WebElement> options2 = slct4.getOptions();
	    for (WebElement webElement2 : options2) {
			if(webElement2.getText().contains("INR")) {
				slct4.selectByIndex(options2.indexOf(webElement2));
				break;
			}
		}
	     
	    WebElement industry = driver.findElementById("createLeadForm_industryEnumId");
	    Select slct3 = new Select(industry);
	    List<WebElement> options1 = slct3.getOptions();
	    for (WebElement webElement1 : options1) {
		 if(webElement1.getText().contains("Real")) {
				slct3.selectByIndex(options1.indexOf(webElement1));
				break;
			}
		}
	    
	    driver.findElementById("createLeadForm_numberEmployees").sendKeys("50");
	    
	    WebElement Ownership = driver.findElementById("createLeadForm_ownershipEnumId");
	    Select slct5 = new Select(Ownership);
	    slct5.selectByValue("OWN_PROPRIETOR");
	    
	    driver.findElementById("createLeadForm_sicCode").sendKeys("123456");
	    driver.findElementById("createLeadForm_tickerSymbol").sendKeys("###");
	    driver.findElementById("createLeadForm_description").sendKeys("Quality-Honesty-Punctuality");
	    driver.findElementById("createLeadForm_importantNote").sendKeys("Work with Satisfaction");
	    driver.findElementById("createLeadForm_primaryPhoneCountryCode").clear();
	    driver.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("+91");
	    driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("044");
	    driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("9940686883");
	    driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("43905096");
	    driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("Sathish Kumar R");
	    driver.findElementById("createLeadForm_primaryEmail").sendKeys("mrskumar86@gmail.com");
	    driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("www.google.com");
	    driver.findElementById("createLeadForm_generalToName").sendKeys("Raman M");
	    driver.findElementById("createLeadForm_generalAttnName").sendKeys("Sastha");
	    driver.findElementById("createLeadForm_generalAddress1").sendKeys("No 163, Ganesa Puram");
	    driver.findElementById("createLeadForm_generalAddress2").sendKeys("Thirukovilur Road");
	    driver.findElementById("createLeadForm_generalCity").sendKeys("Tiruvannamalai");
	    
	    WebElement country = driver.findElementById("createLeadForm_generalCountryGeoId");
	    Select slct6 = new Select(country);
	    slct6.selectByVisibleText("India");
	    
	    WebElement state = driver.findElementById("createLeadForm_generalStateProvinceGeoId");
	    Select slct7 = new Select(state);
	    slct7.selectByVisibleText("TAMILNADU");
	    
	    driver.findElementById("createLeadForm_generalPostalCode").sendKeys("606601", Keys.TAB);
	    driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("606601");
	    driver.findElementByName("submitButton").click();
	    String leadName = driver.findElementById("viewLead_companyName_sp").getText();
	    if(leadName.contains("Sastha"))
	    	System.out.println("New lead Created in the name of: "+leadName);
	    else
	    	System.out.println("New lead not created yet");
	    
	    
	}

}
