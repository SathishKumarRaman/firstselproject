package directCdForTCs;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class DirectCodeforTC003 {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys("Sastha");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(1000);
		driver.findElementByXPath("//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']/div/a").click();
		String title = driver.getTitle();
		if(title.contains("View Lead"))
			System.out.println("Verified the title of the page is: "+title);
		else
			System.out.println("Verified the title of the page is not View Lead: "+title);
		driver.findElementByXPath("//a[text()='Edit']").click();
		driver.findElementById("updateLeadForm_companyName").clear();
		driver.findElementById("updateLeadForm_companyName").sendKeys("Sastha Builders");
		driver.findElementByName("submitButton").click();
		String text = driver.findElementById("viewLead_companyName_sp").getText();
		if(text.contains("Sastha Builders"))
			System.out.println("Verified the changed name appears as: "+text);
		else
			System.out.println("Verified the changed name does not appears as Sastha Builders but as: "+text);
		driver.close();
	}

}
