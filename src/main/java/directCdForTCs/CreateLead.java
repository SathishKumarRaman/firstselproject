package directCdForTCs;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class CreateLead {
	
	//@Test(invocationCount=2, threadPoolSize=2, invocationTimeOut=30000)
	@Test(groups="smoke")
	public void createLd() {
	System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	ChromeDriver driver = new ChromeDriver();
	driver.manage().window().maximize();
	driver.get("http://leaftaps.com/opentaps/control/main");
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	driver.findElementById("username").sendKeys("DemoSalesManager");
	driver.findElementById("password").sendKeys("crmsfa");
	driver.findElementByClassName("decorativeSubmit").click();
	driver.findElementByLinkText("CRM/SFA").click();
	driver.findElementByLinkText("Create Lead").click();
    driver.findElementById("createLeadForm_companyName").sendKeys("Sastha Mosaics");
    driver.findElementById("createLeadForm_firstName").sendKeys("Sastha");
    driver.findElementById("createLeadForm_lastName").sendKeys("Raman");
    driver.findElementByName("submitButton").click();
    //driver.close();
	}

}
