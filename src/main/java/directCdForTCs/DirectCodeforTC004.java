package directCdForTCs;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.chrome.ChromeDriver;

public class DirectCodeforTC004 {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Merge Leads").click();
		driver.findElementByXPath("//img[@alt='Lookup']").click();
		Set<String> windows = driver.getWindowHandles();
		List<String> lst = new ArrayList<>(windows);
		driver.switchTo().window(lst.get(1));
		driver.findElementByXPath("//label[text()='Lead ID:']//following::div/input").sendKeys("10773");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(1000);
		driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a").click();
		driver.switchTo().window(lst.get(0));
		driver.findElementByXPath("(//img[@alt='Lookup'])[2]").click();
		Set<String> window = driver.getWindowHandles();
		List<String> lst1 = new ArrayList<>(window);
		driver.switchTo().window(lst1.get(1));
		driver.findElementByXPath("//label[text()='Lead ID:']//following::div/input").sendKeys("10417");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(1000);
		driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a").click();
		driver.switchTo().window(lst1.get(0));
		driver.findElementByXPath("//a[@class='buttonDangerous']").click();
		driver.switchTo().alert().accept();
		driver.findElementByXPath("//a[text()='Find Leads']").click();
		driver.findElementByXPath("//div[@class='x-form-item x-tab-item']/div/input").sendKeys("10773");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(1000);
		String text = driver.findElementByXPath("//div[@class='x-paging-info']").getText();
		if(text.contains("No records"))
		System.out.println("Verified it say's no records to display");
		else
			System.out.println(text);
		
		
	}

}
